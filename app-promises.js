var express = require('express');
var app = express();
var getSong = require('./src/getSong');

app.get('/songs/:id', function (request, response) {
  getSong(request.params.id).then(function(song) {
    response.json(song);
  }, function(error) {
    response.status(404).json(error);
    // response.json({
    //   error: 'Cant find song'
    // });
  });
});

app.listen(8000);