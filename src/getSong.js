var mysql = require('mysql');

function getSong(id) {
  return new Promise(function (resolve, reject) {
    var connection = mysql.createConnection({
      host: 'itp460.usc.edu',
      user: 'student',
      password: 'ttrojan',
      database: 'music'
    });

    connection.connect();
    connection.query('SELECT * FROM songs WHERE id = ?', [id], function (error, songs) {
      if (error) {
        reject();
      } else {
        if (songs.length === 0) {
          reject({
            errorType: 'No song',
            id: id
          });
        } else {
          resolve(songs[0]);
        }
      }
    });
  });
}

module.exports = getSong;